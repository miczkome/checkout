package biel.michal.task.controller;

import biel.michal.task.service.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import biel.michal.task.model.dto.BasketItem;

import java.util.List;

@RestController
@RequestMapping("/checkout")
public class CheckoutController {
    private final CheckoutService checkoutService;

    @Autowired
    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    @PostMapping(value = "/totalPriceOfItems", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity cartMethod(@RequestBody List<BasketItem> basketItems) {
        return ResponseEntity.ok(checkoutService.countTotalPriceOfItems(basketItems));
    }
}
