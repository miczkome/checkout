package biel.michal.task.logic;

import biel.michal.task.utils.DataValidator;

import java.math.BigDecimal;

public class XForYDiscount implements DiscountStrategy {
    private final BigDecimal specialPrice;
    private final int unitsForSpecialPrice;

    public XForYDiscount(BigDecimal specialPrice, int unitsForSpecialPrice) {
        this.specialPrice = DataValidator.checkValueIsPositive(specialPrice);
        this.unitsForSpecialPrice = DataValidator.checkValueIsPositive(unitsForSpecialPrice);
    }

    @Override
    public BigDecimal count(BigDecimal price, int amount) {
        BigDecimal sum = new BigDecimal(0.0);
        while (amount!=0) {
            if (amount % unitsForSpecialPrice == 0) {
                sum = sum.add(specialPrice);
                amount -= unitsForSpecialPrice;
            } else {
                sum = sum.add(price);
                amount -= 1;
            }
        }
        return sum;
    }
}
