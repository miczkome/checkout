package biel.michal.task.logic;

import java.math.BigDecimal;

public class StrategyContext {
    private DiscountStrategy discountStrategy;

    public StrategyContext(DiscountStrategy discountStrategy) {
        this.discountStrategy = discountStrategy;
    }

    public BigDecimal countTotalItemPrice (BigDecimal price, int amount) {
        return discountStrategy.count(price, amount);
    }
}
