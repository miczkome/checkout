package biel.michal.task.logic;

import java.math.BigDecimal;

public interface DiscountStrategy {

    BigDecimal count(BigDecimal price, int amount);

}
