package biel.michal.task.logic;

import java.math.BigDecimal;

public class NoDiscount implements DiscountStrategy {

    @Override
    public BigDecimal count(BigDecimal price, int amount) {
        return price.multiply(new BigDecimal(amount));
    }
}
