package biel.michal.task.logic;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class StrategyChooser {
    private Map<Integer, DiscountStrategy> allDiscounts = new HashMap<>();

    public DiscountStrategy getStrategyForItemId (int itemId) {
        DiscountStrategy discountStrategy = allDiscounts.get(itemId);
        if(discountStrategy != null) {
            return discountStrategy;
        }
        return new NoDiscount();
    }

    public Map<Integer, DiscountStrategy> getAllDiscounts() {
        return allDiscounts;
    }

    public void setAllDiscounts(Map<Integer, DiscountStrategy> allDiscounts) {
        this.allDiscounts = allDiscounts;
    }
}
