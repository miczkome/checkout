package biel.michal.task.service;

import biel.michal.task.utils.CurrencyHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import biel.michal.task.model.dto.BasketItem;
import biel.michal.task.logic.StrategyChooser;
import biel.michal.task.logic.StrategyContext;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CheckoutService {
    private final StrategyChooser strategyChooser;
    private final ItemService itemService;

    @Autowired
    public CheckoutService(StrategyChooser strategyChooser, ItemService itemService) {
        this.strategyChooser = strategyChooser;
        this.itemService = itemService;
    }

    public BigDecimal countTotalPriceOfItems(List<BasketItem> basketItems){
        BigDecimal totalPrice = new BigDecimal(0.0);
        for (BasketItem basketItem : basketItems) {
            totalPrice = totalPrice.add(countPriceOfOneBasketItem(basketItem));
        }
        return CurrencyHandler.setScaleForCurrencyValue(totalPrice);
    }

    BigDecimal countPriceOfOneBasketItem(BasketItem basketItem) {
        int id = basketItem.getItemId();
        StrategyContext context = new StrategyContext(strategyChooser.getStrategyForItemId(id));
        return context.countTotalItemPrice(itemService.findItemPriceBy(id), basketItem.getAmount());
    }
}
