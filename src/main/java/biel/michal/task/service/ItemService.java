package biel.michal.task.service;

import biel.michal.task.model.Item;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class ItemService {
        private Set<Item> items = new HashSet<>();

        BigDecimal findItemPriceBy(int itemId) {
            Optional<Item> itemInDatabase = items.stream().filter(e -> e.getId() == itemId).findFirst();
            return itemInDatabase.map(Item::getPrice).orElseGet(() -> BigDecimal.valueOf(0.00));
        }

}
