package biel.michal.task.utils;

import java.math.BigDecimal;

public class CurrencyHandler {
    private CurrencyHandler (){}

    public static BigDecimal setScaleForCurrencyValue (BigDecimal price)  {
        return price.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }
}
