package biel.michal.task.utils;

import java.math.BigDecimal;

public class DataValidator {

    private DataValidator () {}

    public static int checkValueIsPositive (int value) {
        if (value <= 0){
            throw new ArithmeticException();
        }
        return value;
    }

    public static BigDecimal checkValueIsPositive (BigDecimal value) {
        if (value.signum() <= 0){
            throw new ArithmeticException();
        }
        return value;
    }

    public static int checkValueIsNotNegative(int value) {
        if (value < 0){
            throw new ArithmeticException();
        }
        return value;
    }
}
