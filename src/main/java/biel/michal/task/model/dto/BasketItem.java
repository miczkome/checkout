package biel.michal.task.model.dto;

import biel.michal.task.utils.DataValidator;

public class BasketItem {
    private int itemId;
    private int amount;

    public BasketItem () {}

    public BasketItem(int itemId, int amount) {
        this.itemId = itemId;
        this.amount = DataValidator.checkValueIsNotNegative(amount);
    }

    public int getItemId() {
        return itemId;
    }

    public void setItem(int itemId) {
        this.itemId = itemId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = DataValidator.checkValueIsNotNegative(amount);
    }
}
