package biel.michal.task.logic;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class XForYDiscountTest {
    private XForYDiscount xForYDiscount;

    @Before
    public void setUp() {
        xForYDiscount = new XForYDiscount(BigDecimal.valueOf(25), 3);
    }

    @Test
    public void testPriceForNoAnyItem() throws Exception {
        int amount = 0;

        BigDecimal result = xForYDiscount.count(BigDecimal.valueOf(15), amount);

        assertEquals(result, BigDecimal.valueOf(0));
    }

    @Test
    public void testPriceForItemSizeLowerSpecialOffer() throws Exception {
        int amount = 2;

        BigDecimal result = xForYDiscount.count(BigDecimal.valueOf(15), amount);

        assertEquals(result, BigDecimal.valueOf(30));
    }

    @Test
    public void testPriceForItemSizeEqualsSpecialOffer() throws Exception {
        int amount = 3;

        BigDecimal result = xForYDiscount.count(BigDecimal.valueOf(15), amount);

        assertEquals(result, BigDecimal.valueOf(25));
    }

    @Test
    public void testPriceForItemSizeBiggerSpecialOffer() throws Exception {
        int amount = 5;

        BigDecimal result = xForYDiscount.count(BigDecimal.valueOf(15), amount);

        assertEquals(result, BigDecimal.valueOf(55));
    }

    @Test
    public void testPriceForItemSizeEqualsTwoTimesSpecialOffer() throws Exception {
        int amount = 6;

        BigDecimal result = xForYDiscount.count(BigDecimal.valueOf(15), amount);

        assertEquals(result, BigDecimal.valueOf(50));
    }

    @Test(expected = ArithmeticException.class)
    public void testItemPriceIsLowerThanZero() throws Exception {
        xForYDiscount = new XForYDiscount(BigDecimal.valueOf(-12), 30);
    }

    @Test(expected = ArithmeticException.class)
    public void testItemPriceIsZero() throws Exception {
        xForYDiscount = new XForYDiscount(BigDecimal.valueOf(0), 30);
    }

    @Test(expected = ArithmeticException.class)
    public void testUnitsRequiresForSpecialOfferAreLowerThanZero() throws Exception {
        xForYDiscount = new XForYDiscount(BigDecimal.valueOf(12), -12);
    }

    @Test(expected = ArithmeticException.class)
    public void testUnitsRequiresForSpecialOfferAreZero() throws Exception {
        xForYDiscount = new XForYDiscount(BigDecimal.valueOf(0), 0);
    }

}