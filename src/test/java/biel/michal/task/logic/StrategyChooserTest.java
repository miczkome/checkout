package biel.michal.task.logic;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Map;

import static org.junit.Assert.*;

public class StrategyChooserTest {
    @InjectMocks
    private StrategyChooser strategyChooser;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getStrategyForItemInMap() throws Exception {
        int itemId = 1;
        Map<Integer, DiscountStrategy> allDiscounts = strategyChooser.getAllDiscounts();
        allDiscounts.put(itemId, new XForYDiscount(new BigDecimal(70), 3));

        DiscountStrategy discountStrategy = strategyChooser.getStrategyForItemId(itemId);

        assertTrue(discountStrategy instanceof XForYDiscount);
    }

    @Test
    public void getStrategyForItemNotInMap() throws Exception {
        int itemId = 1;
        Map<Integer, DiscountStrategy> allDiscounts = strategyChooser.getAllDiscounts();
        allDiscounts.put(itemId, new XForYDiscount(new BigDecimal(70), 3));

        DiscountStrategy discountStrategy = strategyChooser.getStrategyForItemId(2);

        assertTrue(discountStrategy instanceof NoDiscount);
    }

}