package biel.michal.task.logic;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class NoDiscountTest {
    private NoDiscount noDiscount;

    @Before
    public void setUp() throws Exception {
        noDiscount = new NoDiscount();
    }

    @Test
    public void testPriceForNoAnyItem() throws Exception {
        int amount = 0;

        BigDecimal result = noDiscount.count(BigDecimal.valueOf(15), amount);

        assertEquals(result, BigDecimal.valueOf(0));
    }

    @Test
    public void testPriceForNoFewItem() throws Exception {
        int amount = 7;

        BigDecimal result = noDiscount.count(BigDecimal.valueOf(20), amount);

        assertEquals(result, BigDecimal.valueOf(140));
    }

}