package biel.michal.task.utils;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CurrencyHandlerTest {

    @Test
    public void testScaleForZeroCents() throws Exception {
        BigDecimal number = BigDecimal.valueOf(123.00);

        number = number.setScale(2, BigDecimal.ROUND_HALF_EVEN);

        assertEquals(number, CurrencyHandler.setScaleForCurrencyValue(new BigDecimal(123.0)));
    }

    @Test
    public void testScaleForRoundBiggerHalf() throws Exception {
        assertEquals(BigDecimal.valueOf(2.53), CurrencyHandler.setScaleForCurrencyValue(new BigDecimal(2.529)));
    }

    @Test
    public void testScaleForRoundAtHalf() throws Exception {
        assertEquals(BigDecimal.valueOf(2.52), CurrencyHandler.setScaleForCurrencyValue(new BigDecimal(2.525)));
    }

    @Test
    public void testScaleForRoundLowerHalf() throws Exception {
        assertEquals(BigDecimal.valueOf(2.53), CurrencyHandler.setScaleForCurrencyValue(new BigDecimal(2.534)));
    }

    @Test
    public void testScaleForBiggerValue() throws Exception {
        assertEquals(BigDecimal.valueOf(2232.48), CurrencyHandler.setScaleForCurrencyValue(new BigDecimal(2232.48)));
    }


}