package biel.michal.task.utils;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class DataValidatorTest {

    @Test
    public void testIntValueIsPositiveWhenPositiveChecking() throws Exception {
        int value = 3;

        assertEquals(value, DataValidator.checkValueIsPositive(value));
    }

    @Test(expected = ArithmeticException.class)
    public void testIntValueIsZeroWhenPositiveChecking() throws Exception {
        int value = 0;

        assertEquals(value, DataValidator.checkValueIsPositive(value));
    }

    @Test(expected = ArithmeticException.class)
    public void testIntValueIsNegativeWhenPositiveChecking() throws Exception {
        int value = -4;

        assertEquals(value, DataValidator.checkValueIsPositive(value));
    }

    @Test
    public void testIntValueIsPositiveWhenNotNegativeChecking() throws Exception {
        int value = 12;

        assertEquals(value, DataValidator.checkValueIsNotNegative(value));
    }

    @Test
    public void testIntValueIsZeroWhenNotNegativeChecking() throws Exception {
        int value = 0;

        assertEquals(value, DataValidator.checkValueIsNotNegative(value));
    }

    @Test(expected = ArithmeticException.class)
    public void testIntValueIsNegativeWhenWhenNotNegativeChecking() throws Exception {
        int value = -56;

        assertEquals(value, DataValidator.checkValueIsNotNegative(value));
    }

    @Test
    public void testBigDecimalValueIsPositiveWhenPositiveChecking() throws Exception {
        BigDecimal value = BigDecimal.valueOf(3);

        assertEquals(value, DataValidator.checkValueIsPositive(value));
    }

    @Test(expected = ArithmeticException.class)
    public void testBigDecimalValueIsZeroWhenPositiveChecking() throws Exception {
        BigDecimal value = BigDecimal.valueOf(0);

        assertEquals(value, DataValidator.checkValueIsPositive(value));
    }

    @Test(expected = ArithmeticException.class)
    public void testBigDecimalValueIsNegativeWhenPositiveChecking() throws Exception {
        BigDecimal value = BigDecimal.valueOf(-5);

        assertEquals(value, DataValidator.checkValueIsPositive(value));
    }

}