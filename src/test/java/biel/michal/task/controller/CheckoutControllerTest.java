package biel.michal.task.controller;

import biel.michal.task.model.dto.BasketItem;
import biel.michal.task.service.CheckoutService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CheckoutControllerTest {

    @InjectMocks
    private CheckoutController checkoutController;

    @Mock
    private CheckoutService checkoutService;

    private ObjectMapper mapper;
    private MockMvc mockMvc;

    private final MediaType contentType = MediaType.APPLICATION_JSON;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders
                .standaloneSetup(checkoutController)
                .build();
    }

    @Test
    public void testControllerNormalCase() throws Exception {
        List<BasketItem> basketItemList = new ArrayList<>();
        BigDecimal basketItem = BigDecimal.valueOf(12);
        basketItemList.add(new BasketItem(1, 12));
        basketItemList.add(new BasketItem(2, 4));

        String basketListJson = mapper.writeValueAsString(basketItemList);
        Mockito.when(checkoutService.countTotalPriceOfItems(basketItemList)).thenReturn(basketItem);

        mockMvc.perform(post("/checkout/totalPriceOfItems").contentType(contentType).content(basketListJson))
               .andExpect(status().isOk());
    }

    @Test
    public void testControllerEmptyList() throws Exception {
        List<BasketItem> basketItemList = new ArrayList<>();
        BigDecimal basketItem = BigDecimal.valueOf(12);

        String basketListJson = mapper.writeValueAsString(basketItemList);
        Mockito.when(checkoutService.countTotalPriceOfItems(basketItemList)).thenReturn(basketItem);

        mockMvc.perform(post("/checkout/totalPriceOfItems").contentType(contentType).content(basketListJson))
               .andExpect(status().isOk());
    }

    @Test
    public void testControllerWithoutContent() throws Exception {
        List<BasketItem> basketItemList = new ArrayList<>();
        BigDecimal basketItem = BigDecimal.valueOf(12);

        Mockito.when(checkoutService.countTotalPriceOfItems(basketItemList)).thenReturn(basketItem);

        mockMvc.perform(post("/checkout/totalPriceOfItems").contentType(contentType))
               .andExpect(status().is4xxClientError());
    }

@Test
    public void testControllerWhenAmountIsNotInRange() throws Exception {
        List<BasketItem> basketItemList = new ArrayList<>();
        BigDecimal basketItem = BigDecimal.valueOf(12);

        Mockito.when(checkoutService.countTotalPriceOfItems(basketItemList)).thenReturn(basketItem);

        mockMvc.perform(post("/checkout/totalPriceOfItems")
                .contentType(contentType)
                .content("[{\"itemId\":1,\"amount\":-12}]"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

}
