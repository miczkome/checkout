package biel.michal.task.service;

import biel.michal.task.logic.DiscountStrategy;
import biel.michal.task.logic.NoDiscount;
import biel.michal.task.logic.StrategyChooser;
import biel.michal.task.logic.XForYDiscount;
import biel.michal.task.model.dto.BasketItem;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CheckoutServiceTest {

    @InjectMocks
    private CheckoutService testingObject;

    @Spy
    private StrategyChooser strategyChooser;

    @Mock
    private ItemService itemService;

    private final int itemId = 1;
    private  final BigDecimal itemPrice = BigDecimal.valueOf(12);

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCountPriceOfOneBasketItemWithoutDiscount () {
        BasketItem basketItem = new BasketItem(1, 3);
        DiscountStrategy noDiscount = new NoDiscount();

        Mockito.when(strategyChooser.getStrategyForItemId(itemId)).thenReturn(noDiscount);
        Mockito.when(itemService.findItemPriceBy(itemId)).thenReturn(itemPrice);

        assertEquals(BigDecimal.valueOf(36), testingObject.countPriceOfOneBasketItem(basketItem));
    }

    @Test
    public void testCountPriceOfOneBasketItemWithoutAmount () {
        BasketItem basketItem = new BasketItem(1, 0);
        DiscountStrategy noDiscount = new NoDiscount();

        Mockito.when(strategyChooser.getStrategyForItemId(itemId)).thenReturn(noDiscount);
        Mockito.when(itemService.findItemPriceBy(itemId)).thenReturn(itemPrice);

        assertEquals(BigDecimal.valueOf(0), testingObject.countPriceOfOneBasketItem(basketItem));
    }

    @Test
    public void testCountPriceOfOneBasketItemWithDiscount () {
        BasketItem basketItem = new BasketItem(1, 3);
        DiscountStrategy discount = new XForYDiscount(BigDecimal.valueOf(20), 3);

        Mockito.when(strategyChooser.getStrategyForItemId(itemId)).thenReturn(discount);
        Mockito.when(itemService.findItemPriceBy(itemId)).thenReturn(itemPrice);

        assertEquals(BigDecimal.valueOf(20), testingObject.countPriceOfOneBasketItem(basketItem));
    }

    @Test
    public void testCountPriceOfOneBasketItemWithDiscountAndWithoutAmount () {
        BasketItem basketItem = new BasketItem(1, 0);
        DiscountStrategy discount = new XForYDiscount(BigDecimal.valueOf(20), 3);

        Mockito.when(strategyChooser.getStrategyForItemId(itemId)).thenReturn(discount);
        Mockito.when(itemService.findItemPriceBy(itemId)).thenReturn(itemPrice);

        assertEquals(BigDecimal.valueOf(0), testingObject.countPriceOfOneBasketItem(basketItem));
    }

}