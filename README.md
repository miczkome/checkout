# Checkout mechanism
Recruitment task for Pragamanitc Coders by Michał Biel.

### Business requirements:
1. Design and implement market checkout component with readable API that calculates the total price of a number of items.
2. Checkout mechanism can scan items and return actual price (is stateful).
3. Our goods are priced individually.
4. Some items are multi-priced: buy N of them, and they’ll cost you Y cents.
 
  |Item  | Price |  Unit |Special Price|
  |:----:|-----------:|:----:|:--------:|
  |  A   |  40        | 3   | for 70 
  |  B   |  10        | 2   | for 15 
  |  C   |  30        | 4  | for 60
  |  D   |  25        | 2   | for 40
 
### Technical requirements:
1. The output of this task should be a project with buildable production ready service, that can be executed independently of the source code.
2. Project should include all tests, including unit, integration and acceptance tests.
3. The service should expose REST api, without any UI.
4. You can use gradle, maven or any other building tool.
5. It ought to be written in Java 8.
6. If requested, please use Spring or any other framework, otherwise, the choice is yours.

## Getting Started
### System requirements
1. Git
2. Java (version 1.8)
3. Maven

#### To Start App
* Clone the repo: `git clone https://miczkome@bitbucket.org/miczkome/checkout.git`
* Open folder with source code `cd checkout`
* Type in cmd/shell  `mvn package`
* Then type `java -jar ./target/checkout-1.0.jar`

The app should launch properly and be available at http://localhost:8080

## Application design 
### API Method
|URL  | Method |  Params |Data Params| Success Response | Error Response
  |:----:|:----:|:----:|:--------:|:----:|:-------:|
  |/checkout/totalPriceOfItems| POST | None| [{"itemId":1,"amount":12}] |Code: 200,  Content: { 120.00 }| Code: 400

### Application summary
There is only one supported API request that calculates actual price of all items, because I understood the task in this way. I do not implement any method to add/delete etc. items or discounts. Also I do not hardcode any data in the app. There are tests to valid the application.

A list of objects is sent in the request. These DTO objects have two attributes: itemId and amount. This is a better approach for me than sending whole item objects. The content of such a request should be established with people using the API.

Sending the itemId requires getting complete information about the item at backend. I do not implement database lawyer, so there is a method that finds the right item object in the set - in  a real system it would be done using a database query.

I use a strategy pattern to implement items’ discounts. This pattern makes it easier to add new discounts and meets requirements of OCP. There are two strategies to count price, first for a item with ‘buy N of them, and they’ll cost you Y cents’ discount, second for an article without any special offer. 

Strategies are stored in map <itemId, strategy>. If there is no special strategy for itemId, then it is returned a strategy with no any discount. It is acceptable solution in this case and suggests a way to solve the problem of discounts. I would suggest more complex implementation without map, for larger and more stable applications.

